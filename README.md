# toscana/docker-compose/proxy

## Usage

```bash
cp .env.example .env  # and edit .env
docker-compose up --detach
```

## References
* [https://github.com/jwilder/nginx-proxy](https://github.com/jwilder/nginx-proxy)
